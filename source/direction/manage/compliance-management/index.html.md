---
layout: markdown_page
title: "Category Direction - Compliance Management"
---

- TOC
{:toc}

## Compliance Management: Introduction

Thanks for visiting this direction page on Compliance Management in GitLab. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/2302) for this category.

Compliance is a concept that has historically been complex and unfriendly. It evokes feelings of stress, tedium, and a desire to avoid the work entirely. Our goal is to change that paradigm and create a Compliance experience that's simple, friendly, and as frictionless as possible. Managing your compliance program should be easy and give you a sense of pride in your organization, not a stomach ache.

## Problems to solve
Enterprises operating in regulated environments need to ensure the technology they use complies with their internal company policies and procedures, which are largely based on the requirements of a particular legal or regulatory framework (e.g. GDPR, SOC 2, ISO, PCI-DSS, SOX, COBIT, HIPAA, FISMA, NIST) governing their industry. Customers need features that enable them to comply with these frameworks beginning with defining the rules, or controls, for their GitLab environment.

Currently, there's no way for an organization to manage the compliance status for groups and projects. There's no mechanism in place for an organization to know, within GitLab, what groups or projects are subject to particular compliance requirements.

* What success looks like: GitLab administrators and group owners should be able to associate groups and projects with specific compliance frameworks, such as SOC 2, GDPR, HIPAA, SOX, etc. This association should be tied to certain compliance controls that govern how the groups and projects operate.

When customers adhere to internal or external compliance frameworks, often times a need for customization arises. One organization's process for an activity can vary greatly from another's and this can create friction or usability issues. To facilitate better usability within GitLab towards compliance efforts, we'll be introducing features that enable customers to define specific policies or requirements their users and instance should abide by.

* What success looks like: Customers will be able to specify specific rules the instance must follow at an instance, group, project, and user-level to maintain tighter control of their environments. These rules should come from "standard" settings GitLab provides and provide an option to customize those rules as necessary to suit individual customer needs.

In almost all cases, compliance controls for an organization focus on reducing overall risk, enforcing separation of duties, and implementing remediation processes. Without features to support these efforts, administrators for these organizations are faced with few workarounds: they can tolerate higher risk, lock down an instance and manage additional administrative overhead at the price of velocity, or build and maintain layers of automation on their own. For enterprises to thrive, this is a problem that demands a high-quality, native solution in GitLab.

* What success looks like: Customers should be able to transfer internal company processes to GitLab's environment to meet their needs, but without the complexity and headache of massive configuration time.

## Maturity
Compliance Management is currently in the **minimal** state. This is because we don't yet have a way to associate groups and projects with specific compliance frameworks, but there are existing settings and features that support compliance use cases.

* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.

In order to bring Compliance Management to the **viable** state, we will be implementing features that allow GitLab group owners and administrators to assign specific compliance controls to projects based on pre-defined, sensible defaults based on existing compliance framework requirements. These controls should introduce simple, but meaningful controls to govern activity within a project, such as ensuring merge request approval rules are adhered to and cannot be bypassed without explicit approval.

* Please track the Minimal maturity plan in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1982).

Once we've achieved a **viable** version of Compliance Management, achieving a **Complete** level of maturity will involve collecting customer feedback and reacting. Assuming we're on the right track, we'll likely scale the solution in two dimensions:

* Increase the number of settings and features you can affect by assigning a framework to a group or project. 
* Increase the comprehensiveness of "out-of-the-box" Compliace Management to incorporate evidence collection, reporting, and automation of auditing tasks.

Finally, once we've achieved a ruleset that's sufficiently flexible and powerful for enterprises, it's not enough to be able to define these rules - we should be able to measure and confirm that they're being adhered to. Achieving **Lovable** maturity likely means further expansion of the two dimensions above, plus visualizing/reporting on the state of Compliance Management across the instance.

## What's Next & Why

Compliance Management will initially be focused on the SOC2, SOX, and HIPAA compliance frameworks because these three frameworks appear to be some of the most common among GitLab customers. Additionally, the features we build for these frameworks will inherently add value to other organizations who are managing compliance with other frameworks due to the fundamental nature of many requirements the various frameworks share. For example, adding access control features to support HIPAA could potentially satisfy requirements for PCI-DSS and NIST.

We'll be introducing better control at the [project level](https://gitlab.com/groups/gitlab-org/-/epics/2087) with features like [controls definition](https://gitlab.com/gitlab-org/gitlab/issues/36300) and [compliance checks in merge requests](https://gitlab.com/gitlab-org/gitlab/issues/34830). We'll also focus on [group-level](https://gitlab.com/groups/gitlab-org/-/epics/2187) considerations such as [preventing project maintainers from changing critical settings](https://gitlab.com/gitlab-org/gitlab/issues/38051) like merge request approvals.

We'd also like to leverage the [GitLab Control Framework](https://about.gitlab.com/handbook/engineering/security/compliance.html#gitlabs-control-framework-gcf) as a single source of truth. By using the GCF, a group owner or administrator could apply specific GCF controls to a project to support multiple legal and regulatory frameworks as opposed to requiring separate framework assignments. 

Examples of the GCF controls:

* [CM.1.02 - Change Approval](https://about.gitlab.com/handbook/engineering/security/guidance/CM.1.02_change_approval.html) - Ensures things like MR approval settings are set and non-admins or group owners cannot make changes to these settings.
* [RM.1.01 - Risk Assessment](https://about.gitlab.com/handbook/engineering/security/guidance/RM.1.01_risk_assessment.html) - Could auto-generate an issue for each authored MR with a template for assessing the risk and impact of the desired change.

We're currently exploring the GCF as a mapping precedent in this [discovery vision issue](https://gitlab.com/gitlab-org/gitlab/issues/40600) and we encourage you to provide your feedback.

## How you can help
This vision is a work in progress, and everyone can contribute:

* Please comment and contribute in the linked issues and epics on this category page. Sharing your feedback directly on GitLab.com is the best way to contribute to our vision.
* Please share feedback directly via email, Twitter, or on a video call. If you’re a GitLab user and have direct knowledge of your need for compliance and auditing, we’d especially love to hear from you.
* Join our [Compliance Special Interest Group (SIG)](https://gitlab.com/gitlab-org/ux-research/issues/532) where you have a direct line of communication with the PM for Manage:Compliance
